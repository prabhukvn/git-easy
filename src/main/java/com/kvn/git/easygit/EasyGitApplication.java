package com.kvn.git.easygit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.kvn.git.easygit.cloner.GitCloner;

@SpringBootApplication
public class EasyGitApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(EasyGitApplication.class, args);
		GitCloner gitCloner= context.getBean(GitCloner.class);
		gitCloner.cloner();
		
	}

}
