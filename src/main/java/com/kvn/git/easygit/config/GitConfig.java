package com.kvn.git.easygit.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("git")
public class GitConfig {
	
	private List<String> repositoryNames;
	private String username;
	private String password;
	private String remoteBaseUrl;
	private String localBaseUrl;
	private String branchToClone;
	/**
	 * @return the repositoryNames
	 */
	public List<String> getRepositoryNames() {
		return repositoryNames;
	}
	/**
	 * @param repositoryNames the repositoryNames to set
	 */
	public void setRepositoryNames(List<String> repositoryNames) {
		this.repositoryNames = repositoryNames;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the remoteBaseUrl
	 */
	public String getRemoteBaseUrl() {
		return remoteBaseUrl;
	}
	/**
	 * @param remoteBaseUrl the remoteBaseUrl to set
	 */
	public void setRemoteBaseUrl(String remoteBaseUrl) {
		this.remoteBaseUrl = remoteBaseUrl;
	}
	/**
	 * @return the localBaseUrl
	 */
	public String getLocalBaseUrl() {
		return localBaseUrl;
	}
	/**
	 * @param localBaseUrl the localBaseUrl to set
	 */
	public void setLocalBaseUrl(String localBaseUrl) {
		this.localBaseUrl = localBaseUrl;
	}
	/**
	 * @return the branchToClone
	 */
	public String getBranchToClone() {
		return branchToClone;
	}
	/**
	 * @param branchToClone the branchToClone to set
	 */
	public void setBranchToClone(String branchToClone) {
		this.branchToClone = branchToClone;
	}
	
	

}
