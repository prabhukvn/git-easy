/**
 * 
 */
package com.kvn.git.easygit.cloner;

import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.errors.CanceledException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidConfigurationException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.api.errors.RefNotAdvertisedException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.api.errors.WrongRepositoryStateException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kvn.git.easygit.config.GitConfig;

/**
 * @author venkoppu
 *
 */
@Component
public class GitCloner {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	/**
	 * @param args
	 */

	@Autowired
	private GitConfig gitConfig;

	public void cloner() {

		logger.info("####################### Number of Repos :{} ##################",gitConfig.getRepositoryNames().size());
		gitConfig.getRepositoryNames().forEach(repoName -> {

			try {
				logger.info("Repository Names: {}", repoName);
				File file = new File(gitConfig.getLocalBaseUrl() + repoName);
				if (file.exists()) {
					pullCommand(repoName);
				} else {
					cloneRepo(repoName);
				}
			} catch (GitAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
		
		logger.info("############################# End of Process ####################$$");

	}

	public void cloneRepo(String repoName) throws GitAPIException {
		String repositoryUrl = gitConfig.getRemoteBaseUrl() + repoName + ".git";
		CloneCommand cloneCommand = Git.cloneRepository().setURI(repositoryUrl)
				.setDirectory(new File(gitConfig.getLocalBaseUrl() + repoName)).setBranch(gitConfig.getBranchToClone());

		logger.info("Cloning repository {}", repositoryUrl);
		UsernamePasswordCredentialsProvider credentials = new UsernamePasswordCredentialsProvider(
				gitConfig.getUsername(), gitConfig.getPassword());
		cloneCommand.setCredentialsProvider(credentials);
		Git repository = cloneCommand.call();

	}

	public void checkoutRepo(String repoName) {

	}

	public void pullCommand(String repoName) {
		try {
			Repository localRepo = new FileRepository(gitConfig.getLocalBaseUrl() + repoName + "/.git");
			logger.info("Running pull command for: {}", repoName);
			Git git = new Git(localRepo);
			UsernamePasswordCredentialsProvider credentials = new UsernamePasswordCredentialsProvider(
					gitConfig.getUsername(), gitConfig.getPassword());

			PullCommand pullCommand = git.pull();
			pullCommand.setCredentialsProvider(credentials);
			pullCommand.call();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WrongRepositoryStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidRemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CanceledException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RefNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RefNotAdvertisedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoHeadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
