# git-easy
This is a simple spring boot project.

1. Checkout the code and intall into STS (Optional)

2. change application-<profile>.properties with appropriate git repository details

3. and Run the spring boot application with required <profile>.

Result:

This will download all the repositories which are listed in application-<profile>.properties file in the given location.
